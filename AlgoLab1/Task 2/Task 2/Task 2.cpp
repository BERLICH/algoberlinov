﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <locale.h>
#include <Windows.h>

union SignedShort
{
	signed short a;
	struct Bites {
		unsigned short znach : 15;
		unsigned short znak : 1;
	}bites;
}A;

int main()
{
	SetConsoleCP(1251); SetConsoleOutputCP(1251);
	printf("Введіть число:"); scanf_s("%d", &A);
	printf("Використовуючи структуру даних:");
	if (A.bites.znak == 1)
		printf("Число від'ємне\n");
	else
		printf("Число додатне\n");
	printf("Використовуючи логічну операцію: ");
	if (A.a < 0)
		printf("Число від'ємне\n");
	else
		printf("Число додатне\n");
	return 0;
}