﻿#include <stdio.h>
#include <locale.h>
#include <Windows.h>
#include <stdlib.h>
#include <time.h>

struct Date
{
	int years;
	unsigned char months;
	unsigned char days;
	unsigned char hours;
	unsigned char minutes;
	unsigned char seconds;
};
int main()
{
	SetConsoleCP(1251);SetConsoleOutputCP(1251);
	Date p;
	printf("Введіть рік:"); scanf_s("%d", &p.years);
	printf("Введіть місяць:"); scanf_s("%hhu", &p.months);
	printf("Введіть день:"); scanf_s("%hhu", &p.days);
	do {
		printf("Введіть години:");
		scanf_s("%hhu", &p.hours);
		if (p.hours < 1 || p.hours > 23)
			printf("Помилка.Введіть число 1<x<23\n");
	} while (p.hours < 1 || p.hours >23);
	do {
		printf("Введіть хвилини:");
		scanf_s("%hhu", &p.minutes);
		if (p.minutes > 59 || p.minutes < 1)
			printf("Помилка.Введіть число 1<x<59\n");
	} while (p.minutes > 59 || p.minutes < 1);
	do {
		printf("Введіть секунди:");
		scanf_s("%hhu", &p.seconds);
		if (p.seconds < 1 || p.seconds>59)
			printf("Помилка.Введіть число 1<x<59\n");
	} while (p.seconds < 1 || p.seconds>59);

	printf("%d.%d.%d   %d:%d:%d\n", p.days, p.months, p.years,p.hours, p.minutes, p.seconds);
	int value = sizeof(Date);
	printf("Об'єм пам'яті, який займає структура:%d\n", value);
	return 0;
}