﻿#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <locale.h>
#include <Windows.h>

int main()
{
	SetConsoleCP(1251);SetConsoleOutputCP(1251);
	signed char a;
	a = 5 + 127;
	printf("5 + 127 = %d\n", a);
	a = 2 - 3;
	printf("2 - 3 = %d\n", a);
	a = -120 - 34;
	printf("-120 - 34 = %d\n", a);
	a = unsigned char(-5);
	printf("unsigned char(-5) = %d\n", a);
	a = 56 & 38;
	printf("56 & 38 = %d\n", a);
	a = 56 | 38;
	printf("56 | 38 = %d\n", a);
	printf_s("\nЯкі значення мали вийти:\n");
	int b;
	b = 5 + 127;
	printf("5 + 127 = %d\n", b);
	b = 2 - 3;
	printf("2 - 3 = %d\n", b);
	b = -120 - 34;
	printf("-120 - 34 = %d\n", b);
	b = unsigned char(-5);
	printf("unsigned char(-5) = %d\n", b);
	b = 56 & 38;
	printf("56 & 38 = %d\n", b);
	b = 56 | 38;
	printf("56 | 38 = %d\n", b);
	return 0;
}