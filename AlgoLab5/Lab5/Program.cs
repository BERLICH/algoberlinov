﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Diagnostics;

namespace Task1
{
    class Sorting
    {
        static void selectionSortList(LinkedListNode<int> head)
        {
            LinkedListNode<int> temp = head;

            //Проходимся по списку
            while (temp != null)
            {
                LinkedListNode<int> min = temp;
                LinkedListNode<int> after = temp.Next;
                //Проход по несортированому списку
                while (after != null)
                {
                    //Ищем минимальный
                    if (min.Value > after.Value)
                    {
                        min = after;
                    }
                    after = after.Next;
                }
                //Обмениваем элементы
                int tempSort = temp.Value;
                temp.Value = min.Value;
                min.Value = tempSort;
                temp = temp.Next;
            }
        }

        static void insertionSortArray(int[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                int j;
                int temp = array[i];
                //Вставляем элементы в отсортированую часть массива
                for (j = i - 1; j >= 0; j--)
                {
                    if (array[j] < temp)
                    {
                        break;
                    }
                    //Если элемент меньше чем проверяемый то перетаскиваем его вниз
                    array[j + 1] = array[j];
                }
                //Ставим элемент на своё место
                array[j + 1] = temp;
            }
        }

        public class LinkedListInsSort
        {
            public Node head;
            public Node sorted;
            int count;
            public class Node
            {
                public int value;
                public Node next;
                public Node(int val)
                {
                    this.value = val;
                }
            }
            //Вставка елементов в список
            public void push(int val)
            {
                Node newnode = new Node(val);
                newnode.next = head;
                head = newnode;
            }
            public void insertionList(Node headref)
            {
                //Создаём начальный список
                sorted = null;
                Node current = headref;
                //Проходимся по списку и вносим current в отсортированный массив
                while (current != null)
                {
                    Node next = current.next;
                    sortedInsert(current);
                    current = next;
                }
                //меняем ссылку на первый элемент
                head = sorted;
            }
            // метод для вставки елемента в список
            public void sortedInsert(Node newnode)
            {
                // Переносим элемент в начало, если он минимальный
                if (sorted == null || sorted.value >= newnode.value)
                {
                    newnode.next = sorted;
                    sorted = newnode;
                }
                else
                {
                    Node current = sorted;
                    // Ищем элемент, который нужно вставить в узел списка
                    while (current.next != null &&
                            current.next.value < newnode.value)
                    {
                        current = current.next;
                    }
                    //Меняем ссылки на соседние элементы
                    newnode.next = current.next;
                    current.next = newnode;
                }
            }
            public void printList(Node head)
            {
                while (head != null)
                {
                    Console.WriteLine(head.value);
                    head = head.next;
                }
            }
            public void сlear()
            {
                head = null;
                count = 0;
            }
        }

        static void Main(string[] args)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            LinkedListNode<int> listNode = new LinkedListNode<int>(1);
            LinkedList<int> list = new LinkedList<int>();
            list.AddLast(listNode);


            Stopwatch watch;

            Random rnd = new Random();

            Console.WriteLine("\nСортировка выбором двусвязный список\n");

            for (int i = 0; i < 10; i++)
            {
                int random = rnd.Next(0, 50);
                list.AddLast(random);
            }

            selectionSortList(listNode);

            for (int i = 0; i < 10; i++)
            {
                int random = rnd.Next(0, 50);
                list.AddLast(random);
            }

            watch = Stopwatch.StartNew();
            selectionSortList(listNode);
            watch.Stop();

            Console.WriteLine($"Соритровка для 10 элементов — {watch.Elapsed.TotalMilliseconds}ms");

            for (int i = 0; i < 100; i++)
            {
                int random = rnd.Next(0, 50);
                list.AddLast(random);
            }

            watch = Stopwatch.StartNew();
            selectionSortList(listNode);
            watch.Stop();

            Console.WriteLine($"Соритровка для 100 элементов — {watch.Elapsed.TotalMilliseconds}ms");

            for (int i = 0; i < 500; i++)
            {
                int random = rnd.Next(0, 50);
                list.AddLast(random);
            }

            watch = Stopwatch.StartNew();
            selectionSortList(listNode);
            watch.Stop();

            Console.WriteLine($"Соритровка для 500 элементов — {watch.Elapsed.TotalMilliseconds}ms");

            for (int i = 0; i < 1000; i++)
            {
                int random = rnd.Next(0, 50);
                list.AddLast(random);
            }

            watch = Stopwatch.StartNew();
            selectionSortList(listNode);
            watch.Stop();

            Console.WriteLine($"Соритровка для 1000 элементов — {watch.Elapsed.TotalMilliseconds}ms");

            for (int i = 0; i < 2000; i++)
            {
                int random = rnd.Next(0, 50);
                list.AddLast(random);
            }

            watch = Stopwatch.StartNew();
            selectionSortList(listNode);
            watch.Stop();

            Console.WriteLine($"Соритровка для 2000 элементов — {watch.Elapsed.TotalMilliseconds}ms");

            for (int i = 0; i < 5000; i++)
            {
                int random = rnd.Next(0, 50);
                list.AddLast(random);
            }

            watch = Stopwatch.StartNew();
            selectionSortList(listNode);
            watch.Stop();

            Console.WriteLine($"Соритровка для 5000 элементов — {watch.Elapsed.TotalMilliseconds}ms");

            for (int i = 0; i < 10000; i++)
            {
                int random = rnd.Next(0, 50);
                list.AddLast(random);
            }

            watch = Stopwatch.StartNew();
            selectionSortList(listNode);
            watch.Stop();

            Console.WriteLine($"Соритровка для 10000 элементов — {watch.Elapsed.TotalMilliseconds}ms");


            Console.WriteLine("\nСортировка вставками массив\n");

            int[] array = new int[10];

            for (int i = 0; i < array.Length; i++)
            {
                array[i] = rnd.Next(0, 50);
            }
            insertionSortArray(array);

            for (int i = 0; i < array.Length; i++)
            {
                array[i] = rnd.Next(0, 50);
            }

            watch = Stopwatch.StartNew();
            insertionSortArray(array);
            watch.Stop();

            Console.WriteLine($"Сортировка для 10 элементов — {watch.Elapsed.TotalMilliseconds}ms");

            Array.Resize(ref array, 100);

            for (int i = 0; i < array.Length; i++)
            {
                array[i] = rnd.Next(0, 50);
            }

            watch = Stopwatch.StartNew();
            insertionSortArray(array);
            watch.Stop();

            Console.WriteLine($"Сортировка для 100 элементов — {watch.Elapsed.TotalMilliseconds}ms");

            Array.Resize(ref array, 500);

            for (int i = 0; i < array.Length; i++)
            {
                array[i] = rnd.Next(0, 50);
            }

            watch = Stopwatch.StartNew();
            insertionSortArray(array);
            watch.Stop();

            Console.WriteLine($"Сортировка для 500 элементов — {watch.Elapsed.TotalMilliseconds}ms");

            Array.Resize(ref array, 1000);

            for (int i = 0; i < array.Length; i++)
            {
                array[i] = rnd.Next(0, 50);
            }

            watch = Stopwatch.StartNew();
            insertionSortArray(array);
            watch.Stop();

            Console.WriteLine($"Сортировка для 1000 элементов — {watch.Elapsed.TotalMilliseconds}ms");

            Array.Resize(ref array, 2000);

            for (int i = 0; i < array.Length; i++)
            {
                array[i] = rnd.Next(0, 50);
            }

            watch = Stopwatch.StartNew();
            insertionSortArray(array);
            watch.Stop();

            Console.WriteLine($"Сортировка для 2000 элементов — {watch.Elapsed.TotalMilliseconds}ms");

            Array.Resize(ref array, 5000);

            for (int i = 0; i < array.Length; i++)
            {
                array[i] = rnd.Next(0, 50);
            }

            watch = Stopwatch.StartNew();
            insertionSortArray(array);
            watch.Stop();

            Console.WriteLine($"Сортировка для 5000 элементов — {watch.Elapsed.TotalMilliseconds}ms");

            Array.Resize(ref array, 10000);

            for (int i = 0; i < array.Length; i++)
            {
                array[i] = rnd.Next(0, 50);
            }

            watch = Stopwatch.StartNew();
            insertionSortArray(array);
            watch.Stop();

            Console.WriteLine($"Сортировка для 10000 элементов — {watch.Elapsed.TotalMilliseconds}ms");


            Console.WriteLine("\nСортировка вставками двусвязный список\n");

            LinkedListInsSort listInsert = new LinkedListInsSort();

            int size = 10;
            for (int i = 0; i < size; i++)
            {
                int random = rnd.Next(0, 50);
                listInsert.push(random);
            }
            listInsert.insertionList(listInsert.head);


            for (int i = 0; i < size; i++)
            {
                int random = rnd.Next(0, 50);
                listInsert.push(random);
            }

            watch = Stopwatch.StartNew();
            listInsert.insertionList(listInsert.head);
            watch.Stop();

            Console.WriteLine($"Сортировка для {size} элементов — {watch.Elapsed.TotalMilliseconds}ms");

            size = 100;
            for (int i = 0; i < size; i++)
            {
                int random = rnd.Next(0, 50);
                listInsert.push(random);
            }

            watch = Stopwatch.StartNew();
            listInsert.insertionList(listInsert.head);
            watch.Stop();

            Console.WriteLine($"Сортировка для {size} элементов — {watch.Elapsed.TotalMilliseconds}ms");

            size = 500;
            for (int i = 0; i < size; i++)
            {
                int random = rnd.Next(0, 50);
                listInsert.push(random);
            }

            watch = Stopwatch.StartNew();
            listInsert.insertionList(listInsert.head);
            watch.Stop();

            Console.WriteLine($"Сортировка для {size} элементов — {watch.Elapsed.TotalMilliseconds}ms");

            size = 1000;
            for (int i = 0; i < size; i++)
            {
                int random = rnd.Next(0, 50);
                listInsert.push(random);
            }

            watch = Stopwatch.StartNew();
            listInsert.insertionList(listInsert.head);
            watch.Stop();

            Console.WriteLine($"Сортировка для {size} элементов — {watch.Elapsed.TotalMilliseconds}ms");

            size = 2000;
            for (int i = 0; i < size; i++)
            {
                int random = rnd.Next(0, 50);
                listInsert.push(random);
            }

            watch = Stopwatch.StartNew();
            listInsert.insertionList(listInsert.head);
            watch.Stop();

            Console.WriteLine($"Сортировка для {size} элементов — {watch.Elapsed.TotalMilliseconds}ms");

            size = 5000;
            for (int i = 0; i < size; i++)
            {
                int random = rnd.Next(0, 50);
                listInsert.push(random);
            }

            watch = Stopwatch.StartNew();
            listInsert.insertionList(listInsert.head);
            watch.Stop();

            Console.WriteLine($"Сортировка для {size} элементов — {watch.Elapsed.TotalMilliseconds}ms");

            size = 10000;
            for (int i = 0; i < size; i++)
            {
                int random = rnd.Next(0, 50);
                listInsert.push(random);
            }

            watch = Stopwatch.StartNew();
            listInsert.insertionList(listInsert.head);
            watch.Stop();

            Console.WriteLine($"Сортировка для {size} элементов — {watch.Elapsed.TotalMilliseconds}ms");
        }
    }
}
