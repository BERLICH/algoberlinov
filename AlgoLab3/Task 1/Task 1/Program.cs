﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_1
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
            System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            Console.OutputEncoding = Encoding.Unicode; Console.InputEncoding = Encoding.Unicode;

            double x=1, y;
            Console.WriteLine("\n\t--- f(n)=n ---");
            for (int i = 0; i < 50; i ++)
            {
                y = x;
                if (y >= 500)
                {
                    break;
                }
                Console.WriteLine($"f({x})={y}");
                x ++;
            }
            Console.WriteLine("\n\t--- f(n)=log(n) ---");

            x = 1;
            for (int i = 0; i < 50; i++)
            {
                y = Math.Log(x);
                if (y >= 500)
                {
                    break;
                }
                Console.WriteLine($"f({x})={y}");
                x++;
            }

            x = 1;
            Console.WriteLine("\n\t--- f(n)=n*log(n) ---");
            for (int i = 0; i < 50; i++)
            {
                y = x*Math.Log(x);
                if (y >= 500)
                {
                    break;
                }
                Console.WriteLine($"f({x})={y}");
                x++;
            }

            x = 1;
            Console.WriteLine("\n\t--- f(n)=n^2 ---");
            for (int i = 0; i < 50; i++)
            {
                y = Math.Pow(x,2);
                if (y >= 500)
                {
                    break;
                }
                Console.WriteLine($"f({x})={y}");
                x++;
            }

            x = 1;
            Console.WriteLine("\n\t--- f(n)=2^n ---");
            for (int i = 0; i < 50; i++)
            {
                y = Math.Pow(2,x);
                if (y >= 500)
                {
                    break;
                }
                Console.WriteLine($"f({x})={y}");
                x++;
            }

            x = 1;
            Console.WriteLine("\n\t--- f(n)=n! ---");
            for (int i = 0; i < 50; i++)
            {
                y = 1;
                for (int j=1; j<i+2; j++)
                {
                    y *= j;
                }
                if (y >= 500)
                {
                    break;
                }
                Console.WriteLine($"f({x})={y}");
                x++;
            }
        }
    }
}
