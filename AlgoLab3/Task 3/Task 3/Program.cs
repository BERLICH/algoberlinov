﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_3
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
            System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            Console.OutputEncoding = Encoding.Unicode; Console.InputEncoding = Encoding.Unicode;

            int n;
            bool ok;
            Random rnd = new Random();
            Console.WriteLine("\n\t--- Bubble Sort ---");
            do
            {
                Console.Write("Print size n = ");
                ok = int.TryParse(Console.ReadLine(), out n);
                if (ok == false && n > 1000 && n < 1)
                    Console.WriteLine(" Error! Invalid value! Try once more ...");
            } while (!ok);

            double[] arr1 = new double[n];
            double[] arr2 = new double[n];
            double[] arr = new double[n];
            Random rand = new Random();

            Console.Write("\nArray before sort:\n");
            for (int i = 0; i < n; i++)
            {
                arr1[i] = rand.Next(0, 100);
                arr2[i] = rand.Next(1, 99) * 0.001;
                arr[i] = arr1[i] + arr2[i];
                Console.Write($"{arr[i]}\t");
            }
            Console.Write("\n");
            long startTime = DateTime.Now.Ticks; //start
            double temp;
            int s = -1;
            for (int j = 0; j < arr.Length; j++)
                for (int i = 0; i < arr.Length - 1; i++)
                {
                    if (arr[i] * s > arr[i + 1] * s)
                    {
                        temp = arr[i];
                        arr[i] = arr[i + 1];
                        arr[i + 1] = temp;
                    }
                }

            double secondsElapsed = new TimeSpan(DateTime.Now.Ticks - startTime).TotalSeconds; //finish
            Console.WriteLine("\nArray after sort:\n");
            for (int i = 0; i < n; i++)
            {
                Console.Write($"{arr[i]}\t");
            }
            Console.WriteLine($"\nFunction took: {secondsElapsed}");
        }
    }
}