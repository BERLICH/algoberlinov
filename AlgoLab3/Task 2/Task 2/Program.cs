﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_2
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
            System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            Console.OutputEncoding = Encoding.Unicode; Console.InputEncoding = Encoding.Unicode;

            int n;
            bool ok;
            Console.WriteLine("\n\t--- Fibonacci ---");
            do
            {
                Console.Write("Print n = ");
                ok = int.TryParse(Console.ReadLine(), out n);
                if (ok == false && n < 2 && n > 91)
                    Console.WriteLine(" Error! Invalid value! Try once more (1<n<91) ...");
            } while (!ok);
            long startTime = DateTime.Now.Ticks; //start
            int n1 = 0, n2 = 1;
            int sum = 0;

            for (int i = 0; i < n; i++)
            {    
                sum = n1 + n2;
                if (sum == 0)
                    n1++;
                n1 = n2;
                n2 = sum;

            }
            double secondsElapsed = new TimeSpan(DateTime.Now.Ticks - startTime).TotalSeconds; //finish
            Console.WriteLine($"f({n})={sum}");
            Console.WriteLine($"Function took: {secondsElapsed}");
        }
    }
}
