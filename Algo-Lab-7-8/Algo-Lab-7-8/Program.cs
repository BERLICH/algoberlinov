﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
    class Program
    {

        static string[] City = new string[19]
        {
            "Киів", //0
            "Житомир", //1
            "Біла Церква", //2
            "Прилуки", //3
            "Новоград-Волинський", //4
            "Бердичів",//5
            "Шепетівка",//6
            "Рівне", //7
            "Луцьк", //8
            "Вінниця", //9
            "Хмельницький", //10
            "Тернопіль", //11
            "Умань", //12
            "Черкаси", //13
            "Полтава", //14
            "Кременчук", //15
            "Харків", //16
            "Суми", //17
            "Миргород" //18
        };

        static int[,] CityMatrix = new int[19, 19];
        static int[] arr = new int[19];

        public static void BFS(int f)
        {
            bool[] visited = new bool[19];
            string start = City[f];
            visited[f] = true;
            var queue = new Queue<int>();
            queue.Enqueue(f);
            while (queue.Count != 0)
            {
                f = queue.Dequeue();
                for (int i = 0; i < 19; i++)
                {
                    if (CityMatrix[f, i] != 0 && !visited[i])
                    {
                        visited[i] = true;
                        queue.Enqueue(i);
                        arr[i] += (arr[f] + CityMatrix[f, i]);
                        Console.WriteLine(start + " - " + City[i] + ": " + arr[i]);
                    }
                }
            }
        }

        public static void DFS(int count, string str, int res)
        {
            string path;
            bool[] visited = new bool[19];
            if (count == 0)
            {
                path = str + City[count];
            }
            else
                path = str + " > " + City[count];
            if (res != 0)
            {
                path += $"({res})";
            }
            Console.WriteLine(path);
            for (int i = 0; i < 19; i++)
            {
                if (visited[i] == false && CityMatrix[count, i] != 0)
                {
                    DFS(i, path, res = CityMatrix[count, i]);
                }
            }
        }

        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            CityMatrix[0, 1] = 135;
            CityMatrix[1, 4] = 80;
            CityMatrix[4, 7] = 100;
            CityMatrix[7, 8] = 68;
            CityMatrix[5, 9] = 73;
            CityMatrix[9, 10] = 110;
            CityMatrix[10, 11] = 104;
            CityMatrix[1, 6] = 115;
            CityMatrix[0, 2] = 78;
            CityMatrix[2, 12] = 115;
            CityMatrix[2, 13] = 146;
            CityMatrix[13, 15] = 105;
            CityMatrix[2, 14] = 181;
            CityMatrix[14, 16] = 130;
            CityMatrix[0, 3] = 128;
            CityMatrix[3, 17] = 175;
            CityMatrix[17, 18] = 109;

            int k = 0;

            Console.WriteLine("\n\n");
            Console.WriteLine("BFS: \n");
            BFS(k);
            Console.WriteLine("DFS: \n");
            DFS(k, "", 0);
            Console.ReadKey();
        }
    }
}