﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary;

namespace lab3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.WriteLine("Двозв'язний список");
            DoublyLinkedList<int> list = new DoublyLinkedList<int>();
            Console.WriteLine("Додавання початкових елементів. Введіть їх кількість.");
            int amount;
            while (true)
            {
                if (int.TryParse(Console.ReadLine(), out amount))
                {
                    break;
                }
                Console.WriteLine("Error.");
            }
            for (int i = 0; i < amount; i++)
            {
                Console.WriteLine("Введіть число");
                int a;
                if (!int.TryParse(Console.ReadLine(), out a))
                {
                    --i;
                }
                list.Add(a);
            }
            Console.Write("\n");
            if (list.IsEmpty)
            {
                Console.WriteLine("Список пустий");
            }
            else
            {
                foreach (int number in list)
                {
                    Console.WriteLine($"{number}");
                }
            }

            bool close = false;
            do
            {
                Console.WriteLine("\n1. Додати елемент");
                Console.WriteLine("2. Видалити елемент");
                Console.WriteLine("3. Очистити список");
                Console.WriteLine("4. Вихід.");
                int choose;
                while (true)
                {
                    if (int.TryParse(Console.ReadLine(), out choose))
                    {
                        break;
                    }
                    Console.WriteLine("Error.");
                }
                switch (choose)
                {
                    case 1:
                        Console.WriteLine("Введіть елемент.");
                        int elem;

                        if (!int.TryParse(Console.ReadLine(), out elem))
                        {
                            Console.WriteLine("Error.");
                        }
                        else
                            list.Add(elem);
                        break;
                    case 2:
                        if (list.IsEmpty)
                        {
                            Console.WriteLine("Список пустий");
                        }
                        else
                        {
                            Console.WriteLine("Який елемент ви хочете видалити?");
                            int elemDel;

                            if (!int.TryParse(Console.ReadLine(), out elemDel))
                            {
                                Console.WriteLine("Error.");
                            }
                            else
                                list.Remove(elemDel);
                        }
                        break;
                    case 3:
                        if (list.IsEmpty)
                        {
                            Console.WriteLine("Список пустий");
                        }
                        else
                        {
                            list.Clear();
                        }
                        break;

                    case 4:
                        close = true;
                        break;
                    default:
                        Console.WriteLine("Неіснуючий пункт меню.");
                        break;
                }
                foreach (int number in list)
                {
                    Console.WriteLine($"{number}");
                }
            } while (close == false);
        }
    }
}