﻿using System;
using static System.Console;
using System.Text;

namespace Task1
{
    internal class Program
    {
		private static void Main(string[] args)
		{
			Encoding InputEncoding = Encoding.Unicode; Encoding OutputEncoding = Encoding.Unicode;
			const double p = 30000.0;
			ulong a = 16807;
			ulong c = 0;
			ulong m = 4294967295; uint diap = 200; 
			ulong x = 1, j = 0;
			long[] array = new long[(int)p];


			for (int i = 0; i < array.Length; i++)
			{
				x = (a * x + c) % m;
				Write($"{x * diap / (m - 1)} ,");
				array[j] = (long)(x * diap / (m - 1));
				j++;
			}
			long[] arr = new long[(int)diap];
			WriteLine("\n	");
			double[] fr = new double[(int)diap];
			for (int i = 0; i < arr.Length; i++)
			{
				for (int z = 0; z < array.Length; z++)
				{
					if (array[z] == i)
					{
						arr[i]++;
					}
				}
				fr[i] = (arr[i] / p);
				WriteLine($"Число: {i}\tок: {arr[i]}\tfreq {fr[i]}");
			}
			double[] ms = new double[(int)p];
			double ff = 0;
			for (int i = 0; i < fr.Length; i++)
			{
				ff += fr[i] * i;
			}
			WriteLine($"Математичне сподiвання: {ff}");
			double dispersion = 0;
			for (int i = 0; i < fr.Length; i++)
			{
				dispersion += Math.Pow(i - ff, 2) * fr[i];
			}
			Console.WriteLine($"Дiсперсiя = {dispersion}");
			double sqareErr = Math.Sqrt(dispersion);
			Console.WriteLine($"Корiнь = {sqareErr}");
			ReadKey();
		}
	}
}